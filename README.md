#ABOUT THE PROJECT

## What do you need to run

You **MUST** have installed:
- JAVA 8
- MAVEN

To run this test you just need to run the following command:

```
mvn clean test
```

## About the Architecture

This is a really simple project where all tests are described in the `src/test/java` in the **class** *GmailTest.java.

To see a new version of this one including BDD, PageObjects, Fluent Pages and Workflows and the best DesignPatterns, __PLEASE GO TO THE *bonataf*__ project in the parent page of this repository. In there one I have implemented *the same steps* for all the best patterns.

## Running the tests

>Please, DON'T forget to set the right place for your chromedriver in your 

You can run the tests using:

```
./gradlew clean test
```
