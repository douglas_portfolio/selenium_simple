package com.dsbonafe.test.qa;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

public class GMailTest extends TestCase {

	protected static final Logger LOG = LoggerFactory.getLogger(GMailTest.class);

	private WebDriver driver;
	private Properties properties = new Properties();

	private static final long TIMEOUT = 30;
	private static String CHROME_DRIVER_PATH;

	private static final String EMAIL_MSG_AND_SUBJECT = "XOVER TEST";

	private static final String TO_NAME = "to";
	private static final String GMAIL_URL = "https://mail.google.com/";
	private static final String USERNAME_ID = "identifierId";
	private static final String USERNAME_NEXT_ID = "identifierNext";
	private static final String PASSWORD_NEXT_ID = "passwordNext";
	private static final String PASSWORD_XPATH = "//*[contains(@type, 'password')]";
	private static final String TITLE_XPATH = "//title[contains(text(), '%s')]";
	private static final String COMPOSE_XPATH = "//*[contains(text(), 'Compose')]";
	private static final String TEXT_AREA_TO_BEFORE = "//div[contains(@id, ':18h')]";
	private static final String TEXT_AREA_TO = "//textarea[contains(@aria-label, 'To')]";
	private static final String TEXT_AREA_BODY = "//div[@aria-label='Message Body']";
	private static final String MAIL_BODY_SETTINGS = "(//div[contains(@class, 'J-J5-Ji J-JN-M-I-JG')])[last()]";
	private static final String LABEL_SETTING = "(//div[contains(text(), 'Label')])";
	private static final String SEARCH_LABEL_INPUT = "//div[contains(@class,'A0')]//ancestor::div//input";
	private static final String SOCIAL_TAB_PATH = "(//div[contains(text(), 'Social')])[1]";
	private static final String EMAILS_LIST_ITEM = "(((//table[contains(@class, 'F cf zt')])[last()])//following::span)[contains(text(), '%S')]";
	private static final String SEND_BUTTON_XPATH = "//*[@role='button' and text()='Send']";
	private static final String EMAIL_SUBJECT_NAME = "subjectbox";
	private static final String STAR_XPATH = "//div[contains(@title, 'tarred')]";
	private static final String EMAIL_BODY_XPATH = "//div[contains(@class, 'ii gt')]";

	private static final String SEARCH_MAIL_BOX = "//input[contains(@name, 'q')]";

	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		properties.load(new FileReader(new File("test.properties")));
		CHROME_DRIVER_PATH = properties.getProperty("driver.path");
	}

	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test_1_SendEmail() throws Exception {

		// Send email

		// login
		driver.get(GMAIL_URL);
		WebElement userElement = driver.findElement(By.id(USERNAME_ID));
		userElement.sendKeys(properties.getProperty("username"));

		driver.findElement(By.id(USERNAME_NEXT_ID)).click();

		final By pwdElementBy = By.xpath(PASSWORD_XPATH);
		waitUntilElementIsNotPresent(pwdElementBy, TIMEOUT);

		WebElement passwordElement = driver.findElement(pwdElementBy);
		passwordElement.sendKeys(properties.getProperty("password"));
		driver.findElement(By.id(PASSWORD_NEXT_ID)).click();

		// compose
		final By composeBtnBy = By.xpath(COMPOSE_XPATH);
		waitUntilElementIsNotPresent(composeBtnBy, 3 * TIMEOUT);
		WebElement composeElement = driver.findElement(composeBtnBy);
		composeElement.click();

		driver.findElement(By.name(TO_NAME)).clear();
		driver.findElement(By.name(TO_NAME))
				.sendKeys(String.format("%s@gmail.com", properties.getProperty("username")));
		driver.findElement(By.name(EMAIL_SUBJECT_NAME)).sendKeys(EMAIL_MSG_AND_SUBJECT);
		driver.findElement(By.xpath(TEXT_AREA_BODY)).sendKeys(EMAIL_MSG_AND_SUBJECT);

		// labeling
		driver.findElement(By.xpath(MAIL_BODY_SETTINGS)).click();
		waitUntilElementIsNotPresent(By.xpath(LABEL_SETTING), TIMEOUT);
		driver.findElement(By.xpath(LABEL_SETTING)).click();
		waitUntilElementIsNotPresent(By.xpath(SEARCH_LABEL_INPUT), TIMEOUT);
		WebElement searchElement = driver.findElement(By.xpath(SEARCH_LABEL_INPUT));
		searchElement.click();
		searchElement.sendKeys("Social");
		searchElement.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(SEND_BUTTON_XPATH)).click();
		By socialBy = By.xpath(SOCIAL_TAB_PATH);
		waitUntilElementIsNotPresent(socialBy, 2 * TIMEOUT);
		driver.findElement(socialBy).click();

		// Receive and check email

		// search received
		By bySearch = By.xpath(SEARCH_MAIL_BOX);
		String searchLine = String.format("\"label:%s\", \"subject:%s\"", "Social", EMAIL_MSG_AND_SUBJECT);
		WebElement searchBar = driver.findElement(bySearch);
		searchBar.click();
		searchBar.clear();
		searchBar.sendKeys(searchLine);
		searchBar.sendKeys(Keys.ENTER);
		confirmAlertIfVisible();

		// open received
		openReceived();

		// star received
		By byStar = By.xpath(STAR_XPATH);
		waitUntilElementIsNotPresent(byStar, TIMEOUT);
		driver.findElement(byStar).click();

		// assert body
		By bodyBy = By.xpath(EMAIL_BODY_XPATH);
		waitUntilElementIsNotPresent(bodyBy, TIMEOUT);
		Assert.assertTrue(driver.findElement(bodyBy).getText().contains(EMAIL_MSG_AND_SUBJECT));
	}

	public void waitUntilElementIsNotPresent(By locator, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	protected void confirmAlertIfVisible() {
		if (isAlertVisible()) {
			confirmAlert();
		}
	}

	protected boolean isAlertVisible() {
		boolean response = false;
		try {
			String thisWindow = driver.getWindowHandle();
			driver.switchTo().alert();
			driver.switchTo().window(thisWindow);
			response = true;
		} catch (NoAlertPresentException e) {
			LOG.info("Alert was not showed.");
		}
		return response;
	}

	protected void confirmAlert() {
		String mainWindow = driver.getWindowHandle();
		driver.switchTo().alert().accept();
		driver.switchTo().window(mainWindow);
	}

	public void openReceived() {
		By byReceivedEmail = By.xpath(String.format(EMAILS_LIST_ITEM, EMAIL_MSG_AND_SUBJECT));
		waitUntilElementIsNotPresent(byReceivedEmail, TIMEOUT);
		driver.findElement(byReceivedEmail).click();
	}

}
